package com.wstie.gsonapp;
import java.io.StringReader;
import java.io.IOException;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.reflect.TypeToken;
import com.google.gson.stream.JsonReader;
import com.google.gson.stream.JsonToken;

/**
 * Hello world!
 *
 */
public class App 
{
    public static void main( String[] args ) throws IOException
    {
        String opelInsigniaJson;
    	Cars opelInsignia = new Cars();
    	opelInsignia.mark = "Opel";
    	opelInsignia.model = "Insignia";
    	opelInsignia.year = "2011";
    	opelInsignia.color = "black";
    	
    	GsonBuilder builder = new GsonBuilder();
        Gson gson = builder.create();

        System.out.println( "My car: " + opelInsignia.mark + " " + opelInsignia.model );

        // create json from object
        opelInsigniaJson = gson.toJson(opelInsignia);
        System.out.println( "\n" + opelInsigniaJson );
        
        // nested object
    	opelInsignia.engine = new Engine();
    	opelInsignia.engine.power = 150;
    	opelInsignia.engine.capacity = 2.0;
    	opelInsignia.engine.fuel = "diesel";

        opelInsigniaJson = gson.toJson(opelInsignia);
        System.out.println( "\n" + opelInsigniaJson );
        
        // revert json to object
        Cars opel = gson.fromJson(opelInsigniaJson, Cars.class);
        System.out.println( "\nRevert my car: " + opel.color + " " + opel.mark + " " + opel.model );
        
        // parsing json to java token by token
        JsonReader reader = new JsonReader(new StringReader(opelInsigniaJson));
        parseObject(reader);
        
        // create json from array
        int[] ints = {1, 2, 3, 4, 5};
        String intsJson = gson.toJson(ints);

        System.out.println( "\nJson array list:" );
        System.out.println( intsJson );
        
        // revert json to array
        int[] ints2 = new int[5];
        ints2 = gson.fromJson(gson.toJson(ints), int[].class); 
        System.out.println( "\nRevert json array list:" );
        System.out.println( ints2[0] + ", " + ints2[1] + ", " + ints2[2] + ", " + ints2[3] + ", " + ints2[4] );
    }
    
    public static void parseObject (JsonReader reader) throws IOException {
    	reader.beginObject();
        while (reader.hasNext()) {
            JsonToken token = reader.peek();
            if (token.equals(JsonToken.BEGIN_ARRAY))
                parseArray(reader);
            else if (token.equals(JsonToken.END_OBJECT)) {
                reader.endObject();
                return;
            } else
            	parseNonArrayToken(reader, token);
        }
    }

    public static void parseArray(JsonReader reader) throws IOException {
        reader.beginArray();
        while (true) {
            JsonToken token = reader.peek();
            if (token.equals(JsonToken.END_ARRAY)) {
                reader.endArray();
                break;
            } else if (token.equals(JsonToken.BEGIN_OBJECT)) {
                parseObject(reader);
            } else if (token.equals(JsonToken.END_OBJECT)) {
                reader.endObject();
            } else
            	parseNonArrayToken(reader, token);
        }
    }
    
    public static void parseNonArrayToken(JsonReader reader, JsonToken token) throws IOException {
        if (token.equals(JsonToken.NAME))
            System.out.println(reader.nextName());
        else if (token.equals(JsonToken.STRING))
            System.out.println(reader.nextString());
        else if (token.equals(JsonToken.NUMBER))
            System.out.println(reader.nextDouble());
        else
            reader.skipValue();
    }
}
